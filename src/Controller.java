import java.util.Scanner;

public class Controller {
    private static final int CODE_TERMINATE = -1;
    private int durationWork;

    // ввод длительности работы светофора в минутах
    private boolean inputDurationWork() {
        durationWork = 0;
        do {
            System.out.print("Введите время работы светофора в минутах, значение должно быть > 0, для выхода введите \"" +
                    CODE_TERMINATE + "\": ");
            Scanner scanner = new Scanner(System.in);
            if (scanner.hasNextInt()) {
                durationWork = scanner.nextInt();
                if (durationWork == CODE_TERMINATE) {
                    break;
                }
            } else {
                System.out.println("Неверный ввод!!!");
            }
        }
        while (durationWork <= 0);
        return durationWork > 0 ? true : false;
    }

    public void testTrafficLight() {
        if (inputDurationWork()) {
            TrafficLight trafficLight = new TrafficLight();
            if (trafficLight.startAndWork(durationWork)) {
                System.out.println("Последний сигнал светофора был - " + trafficLight.getLastBlinkLamp().getColor());
            }
        }
    }
}
