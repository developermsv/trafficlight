public class Lamp {
    enum COLORS {GREEN, YELLOW, RED};
    private COLORS color;
    private int durationBlink;

    public Lamp(COLORS color,int durationBlink) {
        this.color = color;
        this.durationBlink = durationBlink;
    }

    public void setDurationBlink(int durationBlink) {
        this.durationBlink = durationBlink;
    }

    public int getDurationBlink() {
        return durationBlink;
    }

    public COLORS getColor() {
        return color;
    }
}
