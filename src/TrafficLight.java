public class TrafficLight {
    private static final int INDEX_YELLOW_LAMP = 1;
    private Lamp lastBlinkLamp;
    private static final int DURATION_BLINK_GREEN = 2;
    private static final int DURATION_BLINK_YELLOW = 3;
    private static final int DURATION_BLINK_RED = 5;
    private static final int QUANTITY_LAMPS = 3;
    private Lamp[] lamps = new Lamp[QUANTITY_LAMPS];
    // инициализируем светофор лампами
    public TrafficLight() {
        lamps[Lamp.COLORS.GREEN.ordinal()] = new Lamp(Lamp.COLORS.GREEN, DURATION_BLINK_GREEN);
        lamps[Lamp.COLORS.YELLOW.ordinal()] = new Lamp(Lamp.COLORS.YELLOW, DURATION_BLINK_YELLOW);
        lamps[Lamp.COLORS.RED.ordinal()] = new Lamp(Lamp.COLORS.RED, DURATION_BLINK_RED);
    }

    public Lamp getLastBlinkLamp() {
        return lastBlinkLamp;
    }

    /* имитация работы светофора
     1.GREEN,2.YELLOW,3.RED,4.YELLOW,5.GREEN и т.д. до окончания длительности работы*/
    public boolean startAndWork (int durationWork) {
        if (durationWork <= 0) {
            return false;
        }
        while (durationWork > 0) {
            /* отнимаем от общей длительности работы длительность работы трех ламп
             1 зеленый, 2 желтый,  3 красный*/
            for (Lamp lamp : lamps) {
                lastBlinkLamp = lamp;
                durationWork -= lamp.getDurationBlink();
                printLastBlinkingLamp(durationWork);
                if (durationWork <= 0) {
                    return true;
                }
            }
            lastBlinkLamp = lamps[INDEX_YELLOW_LAMP];
            // отнимаем от общей длительности работы, длительность работы желтой лампы
            durationWork -= lamps[INDEX_YELLOW_LAMP].getDurationBlink();
            printLastBlinkingLamp(durationWork);
        }
        return true;
    }
    // выводим последнюю отработавшую лампу
    private void printLastBlinkingLamp(int durationWork) {
        if (lastBlinkLamp != null) {
            System.out.println(lastBlinkLamp.getColor() + " осталось работать " +
                    (durationWork < 0 ?"меньше минуты...":durationWork + " минут..."));
        }
    }
}
